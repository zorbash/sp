#!/usr/bin/env ruby

require_relative 'parser/core'

Parser::CLI.new.run(args: ARGV.dup, parser: Parser::LogParser)
