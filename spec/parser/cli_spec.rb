# frozen_string_literal: true

require 'tempfile'

RSpec.describe Parser::CLI do
  subject(:cli) { described_class.new }

  describe '#run' do
    subject { cli.run(args: args, parser: parser_class, output_stream: output_stream) }

    let(:output_stream) { StringIO.new }
    let(:parser_class) { double(:parser_class, new: parser) }
    let(:parser) { double(:parser, parse: parsing_output) }
    let(:parsing_output) do
      {
        pageviews: [
          Parser::Metric.new(name: '/home', value: 5),
          Parser::Metric.new(name: '/about', value: 2)
        ],
        uniques: [
          Parser::Metric.new(name: '/home', value: 2),
          Parser::Metric.new(name: '/about', value: 1)
        ]
      }
    end

    context 'without arguments' do
      let(:args) { [] }

      it 'displays an error and exits' do
        expect { subject }
          .to(raise_error(SystemExit) { |error| expect(error.status).to eq(66) }
          .and(output("Log file not provided or not found\n").to_stderr))
      end
    end

    context 'when the log file does not exist' do
      let(:args) { ['this_file_does_not_exist'] }

      it 'displays an error and exits' do
        expect { subject }
          .to(raise_error(SystemExit) { |error| expect(error.status).to eq(66) }
          .and(output("Log file not provided or not found\n").to_stderr))
      end
    end

    context 'when the log file exists' do
      let(:input) { Tempfile.new('foo') }

      let(:args) { [input.path] }

      it 'instantiates a parser of the given class with the file' do
        expect(parser_class)
          .to receive(:new).with(satisfy { |args| args[:file].is_a?(File) && args[:file].path == input.path })

        subject
      end

      it 'calls the #parse method on the parser' do
        expect(parser).to receive(:parse)

        subject
      end

      describe 'output' do
        let(:output_lines) do
          <<~OUTPUT
            ## pageviews ##
            /home 5
            /about 2
            ## uniques ##
            /home 2
            /about 1
          OUTPUT
        end

        it 'prints the results' do
          subject

          expect(output_stream.string).to eql(output_lines)
        end

        context 'when an output_stream is not given' do
          subject { cli.run(args: args, parser: parser_class) }

          it 'prints to the standard output' do
            expect { subject }.to output(output_lines).to_stdout_from_any_process
          end
        end
      end
    end
  end
end
