# frozen_string_literal: true

RSpec.describe Parser::LogParser do
  subject(:parser) { described_class.new(file: file) }

  describe '#parse' do
    subject(:result) { parser.parse }

    let(:file) { logfile(log_lines.shuffle) }
    let(:log_lines) { [log_line(:about), *help_views, *home_views] }
    let(:home_views) { 4.times.map { log_line(:home) } }
    let(:help_views) do
      ip = random_ip_v4

      [log_line(:help), *2.times.map { log_line(:help, ip) }]
    end

    it 'returns pageviews and uniques', :aggregate_failures do
      is_expected.to have_key(:pageviews)
      is_expected.to have_key(:uniques)
    end

    describe ':pageviews' do
      subject { result[:pageviews] }

      it 'sorts in descending order' do
        pageviews = subject.map(&:value)

        expect(pageviews).to eql(pageviews.sort.reverse)
      end

      it 'includes all the paths only once' do
        expect(subject.map(&:name)).to match_array(['/about', '/home', '/help'])
      end

      it 'counts correctly', :aggregate_failures do
        home_metric, help_metric, = subject

        expect(home_metric.name).to eql('/home')
        expect(help_metric.name).to eql('/help')
        expect(home_metric.value).to eql(home_views.size)
        expect(help_metric.value).to eql(help_views.size)
      end

      it 'does not skip pageviews' do
        expect(subject.map(&:value).sum).to eql(log_lines.size)
      end

      context 'with a well-known file' do
        let(:file) { File.new(File.join('spec', 'fixtures', 'webserver.log')) }

        it 'counts correctly', :aggregate_failures do
          expect(subject.map(&:to_s)).to eql(
            [
              '/about/2 90',
              '/contact 89',
              '/index 82',
              '/about 81',
              '/help_page/1 80',
              '/home 78'
            ]
          )
        end
      end
    end

    describe ':uniques' do
      subject { result[:uniques] }

      let(:log_lines) { [*super(), *contact_lines] }
      let(:contact_lines) do
        ip = random_ip_v4

        10.times.map { log_line(:contact, ip) }
      end

      it 'sorts in descending order' do
        uniques = subject.map(&:value)

        expect(uniques).to eql(uniques.sort.reverse)
      end

      it 'includes all the paths only once' do
        expect(subject.map(&:name)).to match_array(['/about', '/home', '/help', '/contact'])
      end

      it 'counts correctly', :aggregate_failures do
        home_metric, help_metric, = subject

        expect(home_metric.name).to eql('/home')
        expect(help_metric.name).to eql('/help')
        expect(subject.find { |metric| metric.name == '/contact' }.value).to eql(1)
      end

      context 'with a well-known file' do
        let(:file) { File.new(File.join('spec', 'fixtures', 'webserver.log')) }

        it 'counts correctly', :aggregate_failures do
          expect(subject.map(&:to_s)).to eql(
            [
              '/help_page/1 23',
              '/contact 23',
              '/home 23',
              '/index 23',
              '/about/2 22',
              '/about 21'
            ]
          )
        end
      end
    end
  end

  def logfile(lines)
    Tempfile.new.tap do |f|
      f << lines.join("\n")
      f.rewind
    end
  end

  def log_line(path, ip = random_ip_v4)
    "/#{path} #{ip}"
  end

  def random_ip_v4
    4.times.map { rand(0..255) }.join('.')
  end
end
