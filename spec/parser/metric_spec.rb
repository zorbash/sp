# frozen_string_literal: true

RSpec.describe Parser::Metric do
  subject(:metric) { described_class.new(name: name, value: value) }

  let(:name) { 'a name' }
  let(:value) { 42 }

  describe '#name' do
    subject { metric.name }

    it { is_expected.to eql(name) }
  end

  describe '#value' do
    subject { metric.value }

    it { is_expected.to eql(value) }
  end

  describe '#to_s' do
    subject { metric.to_s }

    it { is_expected.to eql("#{name} #{value}") }
  end
end
