# About

* Author: Dimitris Zorbas ([zorbash.com](https://zorbash.com) / [github](https://github.com/zorbash))
* Project: A simple log parser

# Requirements

* Ruby >= 2.7
* RSpec 3.9 for the test suite.

# Usage

```
./parser.rb webserver.log
```

# Testing

To run the tests:

```shell
rspec
```

# Linting

```shell
rubocop
```

# Implementation Notes

The requirement for a `parser.rb` file implies that this can be
implemented as a single file without gem dependencies. Therefore this
implementation does not depend on any gems, nor require bundler,
resulting in minimum boilerplate code and faster startup.

The project structure is also affected by the `parser.rb` entry point requirement in
the root directory of the project. Alternatively it could be structured as follows:

```shell
.
├── README.md
├── bin
│   ├── parser
├── lib
│   ├── parser.rb
│   ├── parser
│       ├── log_parser.rb
│       ├── cli.rb
│       ├── metric.rb
```
