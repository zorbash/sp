# frozen_string_literal: true

require 'set'

module Parser
  # A simple web visits log parser
  class LogParser
    # @param file [File] the web log file to parse
    def initialize(file:)
      @file = file
    end

    # @return Hash{pageviews: Array, uniques: Array}
    def parse
      file
        .each_line
        .map { |line| line.split(DELIMITER) }
        .each_with_object({}, &method(:aggregate))
        .yield_self { |aggregation| { pageviews: pageviews(aggregation), uniques: uniques(aggregation) } }
    end

    private

    attr_reader :file

    DELIMITER = ' '

    def aggregate((path, ip), accumulator)
      if accumulator[path]
        accumulator[path][:total] += 1
        accumulator[path][:unique].add ip
      else
        accumulator[path] ||= { total: 1, unique: Set.new([ip]) }
        accumulator[path] ||= { total: 1 }
      end
    end

    def pageviews(aggregation)
      sort(aggregation.map { |k, v| Parser::Metric.new(name: k, value: v[:total]) })
    end

    def uniques(aggregation)
      sort(aggregation.map { |k, v| Parser::Metric.new(name: k, value: v[:unique].size) })
    end

    def sort(metrics)
      metrics.sort_by { |metric| -metric.value }
    end
  end
end
