# frozen_string_literal: true

module Parser
  # The command-line interface of the parser
  class CLI
    # @param args [Array] The command-line arguments
    # @param parser [#parse] A parser class
    # @param output_stream [IO] The object to print output through
    def run(args:, parser:, output_stream: STDOUT)
      @output_stream = output_stream

      parse_arguments!(args)

      print(**parser.new(file: file).parse)
    end

    private

    attr_reader :args, :file, :output_stream

    def parse_arguments!(args)
      @args = args

      filepath, * = args

      file_missing_error! if filepath.nil? || !File.exist?(filepath)

      @file = File.new(filepath)
    end

    def file_missing_error!
      warn 'Log file not provided or not found'

      exit 66 # EX_NOINPUT
    end

    def print(pageviews:, uniques:)
      print_metrics :pageviews, pageviews
      print_metrics :uniques, uniques
    end

    def print_metrics(title, metrics)
      output_stream.puts "## #{title} ##"
      output_stream.puts metrics.map(&:to_s).join("\n")
    end
  end
end
