# frozen_string_literal: true

module Parser
  # A simple PORO / value object for parser metrics
  class Metric
    attr_reader :name, :value

    def initialize(name:, value:)
      raise ArgumentError if name.nil? || value.nil?

      @name = name
      @value = value
    end

    def to_s
      "#{name} #{value}"
    end
  end
end
