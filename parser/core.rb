# frozen_string_literal: true

# The main parser module where autoloading paths are defined
# NOTE: This is a tiny project, no need to use a loaded like zeitwerk
module Parser
  autoload :CLI,       './parser/cli'
  autoload :Metric,    './parser/metric'
  autoload :LogParser, './parser/log_parser'
end
